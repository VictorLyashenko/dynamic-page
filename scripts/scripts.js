'use strict'

var modalButtons = document.querySelector('#btn-modal');
var modal = document.querySelector('#modal');
var close = document.querySelectorAll('#close');
var overlay = document.querySelector('.overlay');
var nav = document.querySelectorAll('#nav');
var menu = document.querySelectorAll('.menu');

//Open modal       
modalButtons.addEventListener('click', function(event) {
    event.preventDefault();
    modal.style.display = 'block';
    overlay.style.display = 'block';
});
// Close modal
close.forEach(function (item) {
    item.addEventListener('click', function(event) {
        event.preventDefault();
        modal.style.display = 'none';
        overlay.style.display = 'none';
        });
});

// accardion nav menu
nav.forEach(function (item, index) {
    item.addEventListener('click', function() {
        menu.forEach(function (item) {
            item.style.display = 'none';
        });
        menu[index].style.display = 'block'
    });
});

//trigger nav menu

// var trigger = false; 
// nav.forEach(function (item, index) {
//     item.addEventListener('click', function() {
//         menu.forEach(function (item) {
//             item.style.display = 'none';
//         });
//         if(trigger === false){
//             menu[index].style.display = 'block';
//             trigger = true;
//         } else if(trigger === true){
//             menu[index].style.display = 'none';
//             trigger = false;
//         };
//     });
// });


